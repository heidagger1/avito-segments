# Start from a Golang base image
FROM golang:latest

# Set the working directory inside the container
WORKDIR /app

# Copy the project files into the working directory
COPY . .

# Build the project
RUN go build -o main ./cmd/main.go

# Expose any necessary ports
EXPOSE 8080

# Set the default command to run when the container starts
CMD ["./main"]