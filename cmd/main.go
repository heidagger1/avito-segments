package main

import (
	"avito-segments/pkg/db"
	"avito-segments/pkg/handler"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

func main() {
	connectionString := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		"0.0.0.0",
		"5432",
		"red",
		"1",
		"mydatabase",
		"disable")
	err := db.InitializeDB(connectionString)
	if err != nil {
		log.Fatal(err)
	}

	router := mux.NewRouter()

	// Segment handlers
	router.HandleFunc("/segment", handler.CreateSegment).Methods("POST")
	router.HandleFunc("/segment/{slug}", handler.DeleteSegment).Methods("DELETE")

	// User handlers
	router.HandleFunc("/user", handler.AddUserToSegment).Methods("POST")
	router.HandleFunc("/user/{user_id}", handler.GetUserActiveSegments).Methods("GET")

	log.Fatal(http.ListenAndServe(":8080", router))

}
