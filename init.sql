CREATE TABLE segments (
                          id SERIAL PRIMARY KEY,
                          segment_name VARCHAR(255) UNIQUE NOT NULL
);

-- Analytics created segments
INSERT INTO segments (segment_name) VALUES ('AVITO_VOICE_MESSAGES');
INSERT INTO segments (segment_name) VALUES ('AVITO_PERFORMANCE_VAS');
INSERT INTO segments (segment_name) VALUES ('AVITO_DISCOUNT_30');
INSERT INTO segments (segment_name) VALUES ('AVITO_DISCOUNT_50');


CREATE TABLE users (
                       id SERIAL PRIMARY KEY,
                       username VARCHAR(255) UNIQUE NOT NULL
);

-- Create 'user_segments' table to represent the many-to-many relationship
CREATE TABLE user_segments (
                               user_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
                               segment_id INTEGER REFERENCES segments (id) ON DELETE CASCADE,
                               PRIMARY KEY (user_id, segment_id)
);

-- Insert sample data into 'users' table
INSERT INTO users (username) VALUES ('1000');
INSERT INTO users (username) VALUES ('1002');
INSERT INTO users (username) VALUES ('1004');

-- Insert sample data into 'user_segments' table to associate segments with users
INSERT INTO user_segments (user_id, segment_id) VALUES (1, 1);
INSERT INTO user_segments (user_id, segment_id) VALUES (1, 2);
INSERT INTO user_segments (user_id, segment_id) VALUES (1, 3);
INSERT INTO user_segments (user_id, segment_id) VALUES (1, 4);
INSERT INTO user_segments (user_id, segment_id) VALUES (2, 1);
INSERT INTO user_segments (user_id, segment_id) VALUES (2, 2);

CREATE TABLE segment_history (
                                 id SERIAL PRIMARY KEY,
                                 user_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
                                 segment_id INTEGER REFERENCES segments (id) ON DELETE CASCADE,
                                 operation VARCHAR(255), -- операция (добавление/удаление)
                                 timestamp TIMESTAMPTZ -- дата и время изменения
);