package table

type Segment struct {
	Id   string `json:"id"`
	Slug string `json:"slug"`
}
