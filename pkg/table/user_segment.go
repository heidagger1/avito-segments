package table

type UserSegmentUpdate struct {
	UserID           string   `json:"user_id"`
	SegmentsToAdd    []string `json:"segments_to_add"`
	SegmentsToDelete []string `json:"segments_to_delete"`
}
