package handler

import (
	"avito-segments/pkg/db"
	"avito-segments/pkg/table"
	"encoding/json"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

// CreateSegment handles the creation of a segment.
func CreateSegment(w http.ResponseWriter, r *http.Request) {
	var segment table.Segment
	err := json.NewDecoder(r.Body).Decode(&segment)
	if err != nil {
		http.Error(w, "Failed to decode request body", http.StatusBadRequest)
		return
	}

	err = db.DB.CreateSegment(segment.Slug)

	if err != nil {
		log.Printf("Failed to create segment: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

// DeleteSegment handles the deletion of a segment.
func DeleteSegment(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	slug := vars["slug"]

	err := db.DB.DeleteSegment(slug)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}
