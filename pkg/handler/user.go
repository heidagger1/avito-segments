package handler

import (
	"avito-segments/pkg/db"
	"avito-segments/pkg/table"
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
)

// AddUserToSegment handles the addition or removal of segments for a user.
func AddUserToSegment(w http.ResponseWriter, r *http.Request) {
	var userSegmentUpdate table.UserSegmentUpdate
	err := json.NewDecoder(r.Body).Decode(&userSegmentUpdate)
	if err != nil {
		http.Error(w, "Failed to decode request body", http.StatusBadRequest)
		return
	}

	err = db.DB.AddUserToSegment(userSegmentUpdate.UserID, userSegmentUpdate.SegmentsToAdd, userSegmentUpdate.SegmentsToDelete)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

// GetUserActiveSegments handles the retrieval of active segments for a user.
func GetUserActiveSegments(w http.ResponseWriter, r *http.Request) {
	userID := mux.Vars(r)["user_id"]
	if userID == "" {
		http.Error(w, "User ID parameter is required", http.StatusBadRequest)
		return
	}

	segments, err := db.DB.GetUserActiveSegments(userID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	response, err := json.Marshal(segments)
	if err != nil {
		http.Error(w, "Failed to marshal response", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}
