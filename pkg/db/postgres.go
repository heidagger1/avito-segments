package db

import (
	"avito-segments/pkg/table"
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"time"
)

type PostgresDB struct {
	db *sql.DB
}

var DB *PostgresDB

// InitializeDB initializes the PostgreSQL database connection.
func InitializeDB(connection string) error {
	db, err := sql.Open("postgres", connection)
	if err != nil {
		return fmt.Errorf("failed to connect to database: %v", err)
	}

	err = db.Ping()
	if err != nil {
		return fmt.Errorf("failed to ping database: %v", err)
	}
	DB = &PostgresDB{
		db: db,
	}

	return nil
}

// CreateSegment inserts a new segment into the segments table.
func (pg *PostgresDB) CreateSegment(slug string) error {
	query := "INSERT INTO segments (segment_name) VALUES ($1)"
	_, err := pg.db.Exec(query, slug)
	if err != nil {
		return fmt.Errorf("failed to create segment: %v", err)
	}
	return nil
}

// DeleteSegment deletes a segment from the segments table based on slug.
func (pg *PostgresDB) DeleteSegment(slug string) error {
	query := "DELETE FROM segments WHERE segment_name = $1"
	_, err := pg.db.Exec(query, slug)
	if err != nil {
		return fmt.Errorf("failed to delete segment: %v", err)
	}
	return nil
}

// AddUserToSegment adds or removes the specified segments for a given user. It also creates a record in segment_history table.
func (db *PostgresDB) AddUserToSegment(userID string, segmentsToAdd []string, segmentsToDelete []string) error {
	tx, err := db.db.Begin()
	if err != nil {
		return err
	}
	defer func() {
		if err != nil {
			tx.Rollback()
			return
		}
		err = tx.Commit()
	}()

	if len(segmentsToAdd) > 0 {

		stmt, err := tx.Prepare("INSERT INTO user_segments(user_id, segment_id) VALUES ($1, $2)")
		if err != nil {
			return err
		}
		defer stmt.Close()

		for _, segmentSlug := range segmentsToAdd {
			segmentID, err := db.GetSegmentIDByName(segmentSlug)
			if err != nil {
				return err
			}

			_, err = stmt.Exec(userID, segmentID)
			if err != nil {
				return err
			}

			_, err = tx.Exec("INSERT INTO segment_history (user_id, segment_id, operation, timestamp) VALUES ($1, $2, $3, $4)",
				userID, segmentID, "добавление", time.Now())
			if err != nil {
				return err
			}
		}

	}

	if len(segmentsToDelete) > 0 {
		stmt, err := tx.Prepare("DELETE FROM user_segments WHERE user_id = $1 AND segment_id = $2")
		if err != nil {
			return err
		}
		defer stmt.Close()

		for _, segmentName := range segmentsToDelete {
			segmentID, err := db.GetSegmentIDByName(segmentName)
			if err != nil {
				return err
			}

			_, err = stmt.Exec(userID, segmentID)
			if err != nil {
				return err
			}

			// Insert record into segment_history for tracking the change
			_, err = tx.Exec("INSERT INTO segment_history (user_id, segment_id, operation, timestamp) VALUES ($1, $2, $3, $4)",
				userID, segmentID, "удаление", time.Now())
			if err != nil {
				return err
			}
		}
	}

	return nil
}

// GetSegmentIDByName is a helper function that takes segment ID and returns segment name.
func (db *PostgresDB) GetSegmentIDByName(segmentSlug string) (string, error) {
	var segmentID string
	err := db.db.QueryRow("SELECT id FROM segments WHERE segment_name = $1", segmentSlug).Scan(&segmentID)
	if err != nil {
		return "", err
	}
	return segmentID, nil
}

// GetUserActiveSegments retrieves the active segments for a given user.
func (db *PostgresDB) GetUserActiveSegments(userID string) ([]table.Segment, error) {
	segments := []table.Segment{}

	// Perform the database query to get user's active segments
	rows, err := db.db.Query("SELECT segments.* FROM segments INNER JOIN user_segments ON segments.id = user_segments.segment_id WHERE user_segments.user_id = $1", userID)
	if err != nil {
		return segments, err
	}
	defer rows.Close()

	// Iterate over query results and populate segments slice
	for rows.Next() {
		var segment table.Segment
		err := rows.Scan(&segment.Id, &segment.Slug)
		if err != nil {
			return segments, err
		}

		segments = append(segments, segment)
	}

	return segments, nil
}
