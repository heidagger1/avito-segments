# avito-segments

## How to use:

There is a docker-compose.yml, so simple docker-compose up should work fine and when it's not there are Dockerfile and Dockerfile.postgres to run service and database separately using docker build

## How to test:

I've been using Postman with these requests. Keep in mind relation users have id and username where id is SERIAL PRIMARY KEY

add new segment
```
POST `http://localhost:8080/segment` 
{
  "slug": "segment-name"
}
```

delete segment
```
DELETE `http://localhost:8080/segment/segment-name`
```

add user to segments
```
POST `http://localhost:8080/user`
{
  "user_id": "user_id",
  "segments_to_add": ["segment_to_add_one", "segment_to_add_another_one"],
  "segments_to_delete": ["segment_to_delete_one"]
}
```
get user segments by id
```
GET `http://localhost:8080/user/user_id`
```
